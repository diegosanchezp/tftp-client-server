import re
import os

from utils import (
    SocketClient, makeRequestPacket,
    sendFile, makeFile, RRQ, WRQ, NETASCII, OCTET,
    LOCALHOST, PORT, STRCODING
)


class Menu:
    menuText = """
        connect 	connect to remote tftp
        mode    	set file transfer mode
        put     	send file
        get     	receive file
        quit    	exit tftp
        verbose 	toggle verbose mode
        trace   	toggle packet tracing
        status  	show current status
        binary  	set mode to octet
        ascii   	set mode to netascii
        rexmt   	set per-packet retransmission timeout
        timeout 	set total retransmission timeout
        ?       	print help information
        """
    mode = "ascii"
    host = LOCALHOST
    port = PORT
    verbose = False

    mapmode = {
        "ascii": NETASCII,
        "image": OCTET,
        "binary": OCTET,
        "octet": OCTET,
        "netascii": NETASCII
    }

    def __init__(self):
        """Inital client setup"""
        self.client = None

        # make a client folder
        os.makedirs("client", exist_ok=True)

    def connect(self, host, por=None):
        """Set the host (and optionally port) for transfers"""
        self.host = host
        if por:
            self.port = por

    def get(self, files):
        """Get a file or set of files from the specified sources"""
        mapfmode = {
            NETASCII: "w",
            OCTET: "wb"
        }

        # Make a client socket if not connected
        if not self.client:
            try:
                self.client = SocketClient(self.host, self.port)
            except ConnectionRefusedError as e:
                print(e)
                return
        netMode = self.mapmode[self.mode]  # encoding
        fmode = mapfmode[netMode]

        # Send each file in files list
        for file in files:

            # Make a RRQ
            self.client.send(makeRequestPacket(RRQ, file, netMode))

            # Make a file in current dir
            makeFile(self.client, f"client/{file}", fmode, netMode)
        # client.close()

    def setMode(self, m):
        """Set the mode for transfers; transfer-mode may be one of ascii or
        binary.  The default is ascii."""

        self.mode = m

    def put(self, files):
        """
        Put a file or set of files to the specified remote file or direc‐
        tory.  The destination can be in one of two forms: a filename on the
        remote host, if the host has already been specified, or a string of
        the form hosts:filename to specify both a host and filename at the
        same time.  If the latter form is used, the hostname specified
        becomes the default for future transfers.  If the remote-directory
        form is used, the remote host is assumed to be a UNIX machine.
        """

        mapfmode = {
            NETASCII: "rt",
            OCTET: "rb"
        }

        if not self.client:
            try:
                self.client = SocketClient(self.host, self.port)
            except ConnectionRefusedError as e:
                print(e)
                return
        netMode = self.mapmode[self.mode]
        fmode = mapfmode[netMode]
        for file in files:
            # Make a write request
            self.client.send(makeRequestPacket(WRQ, f"server/{file}", netMode))
            # Read a file in the client and send it to the tftp server
            try:
                sendFile(self.client, file, fmode, STRCODING)
            except FileNotFoundError:
                print(f"File \"{file}\" not found")
                continue
            except ConnectionResetError:
                print("Server conection lost")
                pass

    def setVerbose(self):
        """Toggle verbose mode."""
        pass


menu = Menu()
while True:
    try:
        command = input("tftp> ")
        args = command.split(' ')
        arglen = len(args)
        if arglen == 1:
            if re.search("^binary|^ascii", args[0]):
                menu.setMode(args[0])
            elif args[0] == "quit":
                if menu.client:
                    menu.client.close()
                break
            elif args[0] == "?":
                print(menu.menuText)
            else:
                print("Invalid command")

        elif arglen == 2:
            # connect host-name
            if args[0] == "connect":
                menu.connect(args[1])
                pass
            elif args[0] == "mode":
                if re.search("^ascii|^netascii|^binary|^image|^octet", args[1]):
                    menu.setMode(args[1])
                else:
                    print(f"{args[1]}: unknown mode")
                    print("usage: mode[ ascii | netascii | binary | image | octet]")
            elif args[0] == "put":
                menu.put(args[1:])
            # get filename
            elif args[0] == "get":
               menu.get(args[1:])

        elif arglen == 3:
            # connect host-name [port]
            if args[0] == "connect":
                menu.connect(args[1], args[2])
            if args[0] == "put":
                menu.put(args[1:])
            elif args[0] == "get":
                menu.get(args[1:])
        elif arglen > 3:
            if args[0] == "put":
                menu.put(args[1:])
            elif args[0] == "get":
               menu.get(args[1:])
        else:
            print("Invalid command")
    except KeyboardInterrupt:
        print('')
