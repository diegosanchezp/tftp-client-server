from utils import *
import re
import pdb
import os

# sudo fuser - k 6021/ tcp

try:
    # make a server folder
    os.makedirs("server", exist_ok=True)
    server = SocketServer(LOCALHOST, PORT)
    while True:

        try:
            fmode = "wt"  # default mode
            encoding = STRCODING

            # recieve request packet
            reqPacket = server.recieve()
            if reqPacket:
                (opcode, *_) = reqPacket
                if opcode == WRQ:
                    # Server responds with first ACK packet
                    server.send(makeAckPacket(0))

                    # Setup encodings and file write modes
                    (_, filename, mode) = reqPacket
                    if re.match(f"^{OCTET}", mode, re.IGNORECASE):
                        fmode = "wb"
                        # encoding = OCTET
                    try:
                        makeFile(server, filename, fmode, encoding)
                    except FileNotFoundError:
                        # Make an error packet and send it to client
                        server.send(makeErrorPacket(1, "File not found."))
                elif opcode == RRQ:
                    # File to read setup
                    fmode = "r"            # read in text mode default
                    encoding = STRCODING
                    (_, filename, mode) = reqPacket
                    if re.match(f"^{OCTET}", mode, re.IGNORECASE):
                        fmode = "rb"
                    try:
                        sendFile(server, filename, fmode, encoding)
                    except FileNotFoundError:
                        # Make an error packet and send it to client
                        server.send(makeErrorPacket(1, "File not found."))
                else:
                    print("error")
        except ConnectionResetError:
            # Create a new socket
            server = SocketServer(LOCALHOST, PORT)
            print("Hey im stuck here")
            continue

except KeyboardInterrupt:
    server.close()
    exit()
