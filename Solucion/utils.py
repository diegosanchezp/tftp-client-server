# =========== Imports ===========

import struct
import socket
import pdb
import base64
import codecs
from abc import ABC, abstractmethod

# =========== Variables ===========

LOCALHOST = "127.0.0.1"                     # The server's hostname or IP address
# PORT = socket.getservbyname('tftp', 'udp')  # The port used by the server
PORT = 6021
PACKET_SIZE = 512
RRQ, WRQ, DATA, ACK, ERROR = range(1, 6)
NETASCII, OCTET = ["netascii", "octet"]
OPCODE_F = "!H"
# STRCODING = 'utf-8'
STRCODING = 'ascii'
ACK_FORMAT = f"{OPCODE_F}I"

# =========== Functions ===========
# Free port: sudo netstat -anp|grep 6021


def makeRequestPacket(opcode, filename, mode):
    s = bytes(filename, STRCODING)
    m = bytes(mode, STRCODING)
    sformat = f"{OPCODE_F}I{len(s)}sI{len(m)}s"
    return struct.pack(sformat, opcode, len(s), s, len(m), m)


def makeDataPacket(blocknum, data):
    """Create data packet"""
    sformat = f"{OPCODE_F}II{len(data)}s"
    return struct.pack(sformat, DATA, blocknum, len(data), data)


def makeAckPacket(blocknum):
    return struct.pack(ACK_FORMAT, ACK, blocknum)


def makeErrorPacket(errorCode, errMsg):
    erM = bytes(errMsg, STRCODING)
    sformat = f"{OPCODE_F}HI{len(erM)}s"
    return struct.pack(sformat, ERROR, errorCode, len(erM), erM)


def decode(fort, packet):
    (c,) = struct.unpack(fort, packet)
    return c


def getOpcode(packet):
    return decode(OPCODE_F, packet)


def decodeMsg(sock, fort):
    packet = sock.recv(struct.calcsize(fort))
    msgLen = decode(fort, packet)
    msg = b''
    while len(msg) < msgLen:
        chunk = sock.recv(msgLen - len(msg))
        if chunk == b'':
            raise RuntimeError("socket connection broken")
        msg = msg + chunk
    return msg.decode(STRCODING)


def decodePacket(sock, packet):
    opcode = getOpcode(packet)
    if opcode == RRQ or opcode == WRQ:
        filename = decodeMsg(sock, "!I")
        mode = decodeMsg(sock, "!I")
        return (opcode, filename, mode)
    elif opcode == DATA:
        # "{OPCODE_F}II{len(data)}s"

        # get blocknum
        packet = sock.recv(struct.calcsize("!I"))
        blocknum = decode("!I", packet)
        # get data, len(data) <= 512
        data = decodeMsg(sock, "!I")
        return (opcode, blocknum, data)
    elif opcode == ACK:
        packet = sock.recv(struct.calcsize("!I"))
        blocknum = decode("!I", packet)
        return (opcode, blocknum)
    elif opcode == ERROR:
        packet = sock.recv(struct.calcsize("!H"))
        ErrorCode = decode("!H", packet)
        ErrMsg = decodeMsg(sock, "!I")
        return(opcode, ErrorCode, ErrMsg)


def makeFile(host, filename, fmode, encoding):
    with open(filename, fmode) as f:
        while True:
            # The next packet from host should be data or error
            resPacket = host.recieve()
            if resPacket:
                (resOp, *_) = resPacket
            else:
                return
            if resOp == DATA:
                (_, block, data) = resPacket
                # write to file
                if fmode == 'wb':
                    dataE = data.encode(STRCODING)
                    f.write(base64.decodebytes(dataE))
                else:
                    f.write(data)
                # host responds with ack packet, that haves the numblock written
                host.send(makeAckPacket(block))
                # End of transfer all bytes hace been sended
                if len(data) < PACKET_SIZE:
                    print(f"File {filename} recieved")
                    return
            elif resOp == ERROR:
                # error end transmision
                (_, errorCode, errorMsg) = resPacket
                print(errorMsg)
                return
            else:
                # error end transmision
                return


def sendFile(host, filename, fmode, encoding):

    fblocknum = 0
    with open(filename, fmode) as f:
        while True:
            data = f.read(PACKET_SIZE)
            if data:
                # Server responds with DATA paquet
                if fmode == 'rb':
                    # Encode binary data
                    host.send(makeDataPacket(
                        fblocknum,
                        base64.b64encode(data)
                    ))
                else:
                    # Encode strings
                    host.send(makeDataPacket(
                        fblocknum,
                        bytes(data, STRCODING)
                    ))
                # Client responds with ACK, check if fblocknum is the same as the one sended
                ackpacket = host.recieve()
                (ackopcode, *_) = ackpacket
                if ackopcode == ACK:
                    (*_, blocknum) = ackpacket
                    if not (blocknum == fblocknum):
                        # error end transmision
                        pass
                    blocknum += 1
                elif ackopcode == ERROR:
                    # error end transmision
                    pass
                else:
                    # error end transmision
                    pass
            else:
                # The file has been completely read
                break

# =========== Classes ===========


class SocketBase(ABC):

    sock = socket.socket(      # TCP socket
        socket.AF_INET,        # IPv4 Internet protocols
        socket.SOCK_STREAM     # reliable, two-way, connection-based byte streams
    )

    @abstractmethod
    def send(self, packet):
        pass

    @abstractmethod
    def recieve(self):
       pass

    def close(self):
        """ Close connection with socket server """
        self.sock.setsockopt(
            socket.SOL_SOCKET,
            socket.SO_REUSEADDR,
            1
        )
        self.sock.close()


class SocketServer(SocketBase):
    """TCP socket server"""

    def __init__(self, host, port):
        self.sock.bind((host, port))   # Assing adress to socket server
        self.sock.listen(1)            # accept incoming connection requests
        # Wait for an incoming connection
        (self.client, self.clientaddr) = self.sock.accept()

    def send(self, packet):
        """Send data packet to client socket"""

        self.client.sendall(packet)

    def recieve(self):
        """Recieve data from the client"""

        # First recieve to look opcode
        packet = self.client.recv(struct.calcsize(OPCODE_F))
        if packet:
            return decodePacket(self.client, packet)


class SocketClient(SocketBase):
    """TCP Sockek client """

    def __init__(self, host, port):
        self.sock.connect((host, port))

    def send(self, packet):
        """ Send packet to server socket """
        self.sock.sendall(packet)

    def recieve(self):
        """ Recieve a packet from server socket """
        # First recieve to look opcode
        bpacket = self.sock.recv(struct.calcsize(OPCODE_F))
        if bpacket:
            dpacket = decodePacket(self.sock, bpacket)

            # Client should never recieve request packet
            (opcode, *_) = dpacket
            if opcode == RRQ or opcode == WRQ:
                # error
                raise RuntimeError("Request packet recieved on client")
            else:
                return dpacket
